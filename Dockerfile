FROM resin/rpi-raspbian

RUN apt-get update && \
    apt-get install -y python python-pip python-webpy python-requests python-smbus \
    python-gst-1.0 python-gst0.10 python-serial python-apscheduler python-dev \
    python-imaging gstreamer0.10-plugins-good gstreamer0.10-plugins-ugly \
    python-rpi.gpio gstreamer0.10 gstreamer0.10-alsa

COPY src /app

EXPOSE 8080/tcp

CMD cd /app && python ./start.py
